/*
 * Unipolar stepper motor speed and direction control with Arduino.
 * Full step control.
 * This is a free software with NO WARRANTY.
 * https://simple-circuit.com/
 */

// include Arduino stepper motor library
#include <Stepper_DG.h>

#include <Encoder.h>
#include <Wire.h>
#include <U8x8lib.h>

// display..
// i2c no reset 
U8X8_SH1106_128X64_NONAME_HW_I2C  u8x8(-1);


int have_display=0;
// DT to pin 3(INT2) and CLK to pin 4 => CC increase, CCW = decrease
Encoder myEnc(3, 4);

// Switch interupt to pin 2 (INT1)
const byte switchPin = 2;

// number of half steps
#define STEPS 800

// create an instance of the up_step class
// provide the pins, so - steps is close, + steps is open
up_step stepper(STEPS,9,8,7,6);


#define MAXSPEED 75

#define FULL_RANGE_P 70
#define FULL_RANGE_T 700
#define FULL_RANGE   7000

// make sure in home it is fully closed!
#define EXTRA_RANGE 200

int position;
long oldPosition  = MAXSPEED;

const int button =  4; // direction control button is connected to Arduino pin 4

int direction_ = 0;
int speed_; 

int oldDirection =0;
int last_dir=-1;

void handleSwitch() 
{  
  if (oldDirection==direction_)
  {
  if (direction_== 0)
  { 
      last_dir=direction_= last_dir *-1;
  }  
  else
    direction_= 0;
  }
}

void print_speed()
{
  String s;
  u8x8.setFont(u8x8_font_chroma48medium8_r);
  u8x8.drawString(0,4,"speed <<");
   s= String(myEnc.read());
  int str_len = s.length() + 1; 
  char char_array[17];
  s.toCharArray(char_array, str_len);
  u8x8.drawString(8,4,"  >> rpm");
  u8x8.drawString(8,4,char_array);
}

void print_valve()
{
   String progress;
      if (direction_ ==1) 
      { // opening
        progress="Opening @ ";
      }
      else if (direction_==-1)
      { //closing 
         progress="Closing @ ";
      } 
      else
      {
        progress="  Valve @ ";
      }
      progress= progress+ String(position/FULL_RANGE_P) + "%    ";
      u8x8.setFont(u8x8_font_chroma48medium8_r);
      int str_len = progress.length() + 1; 
      char char_array[17];
      progress.toCharArray(char_array, str_len);
      u8x8.drawString(0,6,char_array);
}

void clear_valve(void)
{
      u8x8.setFont(u8x8_font_chroma48medium8_r);
      u8x8.drawString(0,6,"                ");
}


void setup() 
{
  Serial.begin(115200);
 

 // init display


  u8x8.begin();

  u8x8.setFont(u8x8_font_px437wyse700a_2x2_r);
  u8x8.drawString(0, 0, "*-EEVT-*");


  u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
  u8x8.drawString(0, 4, "HOMING..");
 

  // configure button pin as input with internal pull up enabled
  pinMode(button, INPUT_PULLUP);
  // switch
  pinMode(switchPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(switchPin), handleSwitch, RISING);
  
  Serial.println("Electronic Expansion Valve Test");
  Serial.println("-------------------------------");
  Serial.println("");
  Serial.println("=> homing Close valve (~10sec)");
  stepper.setSpeed(70);
  int i;

  for (i=10;i>0;i--)
  {
    Serial.print(i);
    Serial.print(".");
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.drawString(11-i,6,"." );
    stepper.step(-(FULL_RANGE_T+EXTRA_RANGE));
  }
  Serial.println("");
  myEnc.write(10); 
  position=0;
    u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
  u8x8.drawString(0, 4, "        ");

  u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
  u8x8.drawString(0, 2, "|CLOSED|");
  print_speed();
  clear_valve();
  u8x8.drawString(0,6,"Press Key 2 Open" );
  Serial.println("Ready");
  stepper.sleep();
}


void loop()
{

  if (direction_ != oldDirection)
  {
     clear_valve();

    if (direction_==1)
    {
      Serial.println("\n--Open Valve--");
      u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
      u8x8.drawString(0, 2, ">OPENING"); 
    }
    else if (direction_==-1)
    {
      Serial.println("\n--Close Valve--");
      u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
      u8x8.drawString(0, 2, "CLOSING<"); 
    }
    else 
    {
      Serial.println("\n--STOP--");
      u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
      u8x8.drawString(0, 2, "||STOP||"); 
      stepper.sleep();
    }
    oldDirection=direction_;
    print_valve();
  }

  long newPosition = myEnc.read();
  if (newPosition != oldPosition)
  {
    if (newPosition<0)
    {
        newPosition=0;
        myEnc.write(newPosition); 
    }
    if (newPosition>(MAXSPEED))
    {
        newPosition=MAXSPEED;
        myEnc.write(newPosition); 
    }
     oldPosition = newPosition;
     speed_=newPosition;
     stepper.setSpeed(speed_);
     Serial.print("\nspeed[rpm]: ");
     Serial.println(speed_);
     print_speed();
  }
  if (speed_!=0 && direction_!=0 && direction_==oldDirection)
  {
    // move the stepper motor
    int steps=direction_*FULL_RANGE_P;
    // limit within range
    if (position+steps>FULL_RANGE) steps=FULL_RANGE-position;
    if ((position+steps)<0) steps=0-position;
    // step it
    stepper.step(steps);
    position=position+steps;
    // display position
    if ((position%FULL_RANGE_P)==0) 
    {
    
      Serial.print("position (%)");
      Serial.print(position/FULL_RANGE_P);
      Serial.print("  \r");
     
      print_valve();
    }
    //Stop
    if (position==FULL_RANGE) 
    {
      oldDirection=direction_=0;
      stepper.sleep();
      Serial.print("\nFully Open => STOPPED @ ");
      Serial.println("FULL_RANGE");
      u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
      u8x8.drawString(0, 2, "--OPEN--"); 
      clear_valve();
      print_valve();
    }
    if (position==0) 
    {
      oldDirection=direction_=0;
      stepper.sleep();
      Serial.println("\nClosed => STOPPED @ 0");
      u8x8.setFont(u8x8_font_px437wyse700b_2x2_r);
      u8x8.drawString(0, 2, "|CLOSED|"); 
      clear_valve();
      print_valve();
    }
  
  }
}
