# EEV test

## purpose

Test and Electronic Expansion Valve.

Initialy design for LGK-31

## Design requirements

- 9V batterie
- portable
- small display indicating operation


Testbed to learn about EEV

## Mechanical

mechanical actuator, stepmotor with reduction to drive screw actuator, needs 8800 steps to go from in to out.

when mounted the valve, only around 7000steps, we do 200 more for some extra safety, to get correct open/close.


## electrical

### STEP DRIVER

|Arduino Pin|IN|ULN2803|OUT|EEV-pin|STEP Function|
|---:|---:|:---:|:---|:---:|:---:|
|6|1|-->>--|18|1|A|
|7|2|-->>--|17|2|B|
|8|3|-->>--|16|3|C|
|9|4|-->>--|15|4|D|
|GND|5|-->>--|14|-||
|GND|6|-->>--|13|-||
|GND|7|-->>--|12|-||
|GND|8|-->>--|11|-||
|GND|9||10|5 & 6| +9V |

### ENCODER

|Arduino Pin|Encoder|Function|Interupt|
|---|:---:|---|---:|
|GND|1|GND|-|
|VCC|2|+5V|-|
|2|3|SWITCH|INT1|
|3|4|DT|INT2|
|4|5|CLK|-|

### BAT

|BAT|Arduino pin|ULN2803|EEV|Function|
|---|:---:|---|---:|---|
|-|GND|9|GND|GND|
|+|RAW|10| 5 & 6|+9V|

### display

|OLED|ARDUINO|Function|
|---|---|---|
|1|VCC|+5V|
|2|GND|i2c-GND|
|3|A5|i2c-SCK|
|4|A4|i2c-SDA|
